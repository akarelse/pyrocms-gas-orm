<?php
namespace Model;

use \Gas\Core;
use \Gas\ORM;

class Ormtest extends ORM
{
    
    public $primary_key = 'id';
    
    function _init() {
        
        // self::$relationships = array (
        // 	'blog'	=>	ORM::has_many('\\Model\\Blog')
        // );
        
        self::$fields = array(
            'id' => ORM::field('auto[10]') ,
            'name' => ORM::field('char[64]') ,
            'slug' => ORM::field('char[255]') ,
        );
    }
}
