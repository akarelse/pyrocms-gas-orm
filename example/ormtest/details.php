<?php defined('BASEPATH') or exit('No direct script access allowed');

class Module_Ormtest extends Module {

	public $version = '2.1';

	public function info()
	{
		return array(
			'name' => array(
				'en' => 'ormtest'
			),
			'description' => array(
				'en' => 'This is a PyroCMS module ormtest.'
			),
			'frontend' => TRUE,
			'backend' => TRUE,
			'menu' => 'content', // You can also place modules in their top level menu. For example try: 'menu' => 'ormtest',
			'sections' => array(
				'items' => array(
					'name' 	=> 'ormtest:items', // These are translated from your language file
					'uri' 	=> 'admin/ormtest',
						'shortcuts' => array(
							'create' => array(
								'name' 	=> 'ormtest:create',
								'uri' 	=> 'admin/ormtest/create',
								'class' => 'add'
								)
							)
						)
				)
		);
	}

	public function install()
	{
		$this->dbforge->drop_table('ormtest');
		

		$ormtest = array(
                        'id' => array(
									  'type' => 'INT',
									  'constraint' => '11',
									  'auto_increment' => TRUE
									  ),
						'name' => array(
										'type' => 'VARCHAR',
										'constraint' => '100'
										),
						'slug' => array(
										'type' => 'VARCHAR',
										'constraint' => '100'
										)
						);



		$this->dbforge->add_field($ormtest);
		$this->dbforge->add_key('id', TRUE);

		if($this->dbforge->create_table('ormtest'))
		{
			return TRUE;
		}
	}

	public function uninstall()
	{
		$this->dbforge->drop_table('ormtest');
		
			return TRUE;
		
	}


	public function upgrade($old_version)
	{
		// Your Upgrade Logic
		return TRUE;
	}

	public function help()
	{
		// Return a string containing help info
		// You could include a file and return it here.
		return "No documentation has been added for this module.<br />Contact the module developer for assistance.";
	}
}
/* End of file details.php */
