<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * This is a ormtest module for PyroCMS
 *
 * @author 		Jerel Unruh - PyroCMS Dev Team
 * @website		http://unruhdesigns.com
 * @package 	PyroCMS
 * @subpackage 	ormtest Module
 */
class Admin extends Admin_Controller
{
	protected $section = 'items';
	
	public function __construct() {
		parent::__construct();
		
		// Load all the required classes
		
		$this->load->library('form_validation');
		$this->load->library('gas');
		$this->lang->load('gas');
		$this->lang->load('ormtest');
		$this->item_validation_rules = array(
			array(
				'field' => 'name',
				'label' => 'Name',
				'rules' => 'trim|max_length[100]|required'
			) ,
			array(
				'field' => 'slug',
				'label' => 'Slug',
				'rules' => 'trim|max_length[100]|required'
			)
		);
		
		// We'll set the partials and metadata here since they're used everywhere
		$this->template->append_js('module::admin.js')->append_css('module::admin.css');
	}
	
	/**
	 * List all items
	 */
	public function index() {
		$data = array();
		
		// here we use MY_Model's get_all() method to fetch everything
		// $data->items = $this->ormtest_m->get_all();
		$data['posts'] = Model\Ormtest::all();
		
		// print_r($data['posts']);
		
		foreach ($data['posts'] as $value) {
			print ($value->name);
		}
	}
	
	public function create() {
		$data = array();
		if ($input = $this->input->post()) {
			
			$post = new Model\Ormtest();
			$post->name = $input['name'];
			$post->slug = $input['slug'];
			$post->save(FALSE);
		}
		
		// Build the view using ormtest/views/admin/form.php
		$this->template->title($this->module_details['name'], lang('ormtest.new_item'))->build('admin/form', $data);
	}
}
