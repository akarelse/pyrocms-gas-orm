# Gas-ORM for pyrocms and codeigniter

A lighweight and easy-to-use ORM for CodeIgniter. Gas was built specifically for CodeIgniter app. It uses CodeIgniter Database packages, a powerful DBAL which support numerous DB drivers. Gas ORM provide a set of methods that will map your database tables and its relationship, into accesible object.

In this case somewhat modded for ussage with pyrocms, some extra posebillitys and removed some bugs.

[orginal gas orm](https://github.com/toopay/gas-orm)

# instructions. At least in the pyrocms version.
- Put gas.php library in library folder.
- put: main file structure and folders in in third_party/gas, the library looks for these files there. (minimal: classes, interfaces, template, bootstrap.php)
- make a gas.php in config folder of your app see example module.
- make sure you make the database via the details.php of your app make sure you make the models via the gas orm road, this is nog tested with models generation or with database generation.
- dont load the models with $this->load->model gas orm will take care of loading the models.