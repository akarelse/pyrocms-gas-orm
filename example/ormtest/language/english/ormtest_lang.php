<?php
//messages
$lang['ormtest:success']			=	'It worked';
$lang['ormtest:error']			=	'It didn\'t work';
$lang['ormtest:no_items']		=	'No Items';

//page titles
$lang['ormtest:create']			=	'Create Item';

//labels
$lang['ormtest:name']			=	'Name';
$lang['ormtest:slug']			=	'Slug';
$lang['ormtest:manage']			=	'Manage';
$lang['ormtest:item_list']		=	'Item List';
$lang['ormtest:view']			=	'View';
$lang['ormtest:edit']			=	'Edit';
$lang['ormtest:delete']			=	'Delete';

//buttons
$lang['ormtest:custom_button']	=	'Custom Button';
$lang['ormtest:items']			=	'Items';
?>